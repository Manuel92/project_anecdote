<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kid extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'anecdote_id',
    ];

    public function Kid(){
        return $this->hasMany('App\User');
    }
}
