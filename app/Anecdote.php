<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anecdote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'image',
        'video',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function Kid(){
        return $this->hasMany('App\User');
    }
}
