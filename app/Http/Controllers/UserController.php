<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getAll(){

        $users = User::with(['UserType'])->get();

        return Response::json($users,200);
    }

    public function getById($id){
        $users = User::with(['UserType'])->find($id);
        return Response::json($users,200);
    }

    public function updateUsers($id ,Request $request){
        $user = User::find($id);
        $user->username = $request->username;
        $user->created_by = $request->created_by;
        $user->updated_by = $request->updated_by;
        $user->deleted_by = $request->deleted_by;
        $user->user_type_id = $request->user_type_id;
        $user->kid_id = $request->kid_id;
        $user->password = Hash::make($request->password);

        $user->save();
        return Response::json($user,200);
    }

    public function createUsers(Request $request){
        $user = new User;
        $user->username = $request->username;
        $user->created_by = $request->created_by;
        $user->updated_by = $request->updated_by;
        $user->deleted_by = $request->deleted_by;
        $user->user_type_id = $request->user_type_id;
        $user->kid_id = $request->kid_id;
        $user->password = Hash::make($request->password);

        $user->save();
        return Response::json($user,200);
    }
    public function deleteUser($id){
        User::destroy($id);

        return Response::json("Votre utilisateur a bien été supprimer",200);
    }

}
