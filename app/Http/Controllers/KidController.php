<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KidController extends Controller
{
    public function getAll(){

        $kids = Kid::with(['UserType'])->get();

        return Response::json($kids,200);
    }

    public function getById($id){
        $kids = kid::with([''])->find($id);
        return Response::json($kids,200);
    }

    public function updateKid($id ,Request $request){
        $kid = Kid::find($id);
        $kid->name = $request->name;
        $kid->user_id = $request->user_id;
        $kid->anecdote_id = $request->anecdote_id;
        

        $kid->save();
        return Response::json($kid,200);
    }

    public function createkid(Request $request){
        $kid = new Kid;
        $kid->name = $request->name;
        $kid->user_id = $request->user_id;
        $kid->anecdote_id = $request->anecdote_id;

        $kid->save();
        return Response::json($kid,200);
    }
    public function deleteKid($id){
        kid::destroy($id);

        return Response::json("Votre enfant a bien été supprimer",200);
    }
}
