<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnecdoteController extends Controller
{
    public function getAll(){

        $anecdotes = Anecdote::with(['UserType'])->get();

        return Response::json($anecdotes,200);
    }

    public function getById($id){
        $anecdotes = Anecdote::with([''])->find($id);
        return Response::json($anecdotes,200);
    }

    public function updateAnecdote($id ,Request $request){
        $anecdote = Anecdote::find($id);
        $anecdote->description = $request->description;
        $anecdote->image = $request->image;
        $anecdote->video = $request->video;
        $anecdote->created_by = $request->created_by;
        $anecdote->updated_by = $request->updated_by;
        $anecdote->deleted_by = $request->deleted_by;

        $anecdote->save();
        return Response::json($anecdote,200);
    }

    public function createAnecdote(Request $request){
        $anecdote = new Anecdote;
        $anecdote->description = $request->description;
        $anecdote->image = $request->image;
        $anecdote->video = $request->video;
        $anecdote->created_by = $request->created_by;
        $anecdote->updated_by = $request->updated_by;
        $anecdote->deleted_by = $request->deleted_by;

        $anecdote->save();
        return Response::json($anecdote,200);
    }
    public function deleteAnecdote($id){
        Anecdote::destroy($id);

        return Response::json("Votre anecdote a bien été supprimée",200);
    }
}
