<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\UserType;

class UserTypeController extends Controller
{
    public function getAll(){

        $userTypes = UserType::all();

        return Response::json($userTypes,200);
    }

    public function getById($id){
        $userTypes = UserType::find($id);
        return Response::json($userTypes,200);
    }

    public function updateUserType($id ,Request $request){
        $userType = UserType::find($id);
        $userType->name = $request->name;

        $userType->save();
        return Response::json($userType,200);
    }

    public function createUserType(Request $request){
        $userType = new UserType;
        $userType->name = $request->name;

        $userType->save();
        return Response::json($userType,200);
    }
    
    public function deleteUserType($id){
        UserType::destroy($id);

        return Response::json("Votre type d'utilisateur a bien été supprimer",200);
    }
}
