<?php

use Illuminate\Database\Seeder;
use App\Kid;

class KidsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kids = 
        [
                    [
                        "name" =>"jojo",
                        "user_id"=>"1",
                        "anecdote_id"=>"1",
                    ],

                    [
                        "name" =>"jaja",
                        "user_id"=>"2",
                        "anecdote_id"=>"2",
                    ],

                    [
                        "name" =>"gege",
                        "user_id"=>"3",
                        "anecdote_id"=>"3",
                    ],
                    [
                        "name" =>"dede",
                        "user_id"=>"4",
                        "anecdote_id"=>"4",
                    ]
                    
        ];

        foreach($kids AS $kid):
            Kid::create($kid);
        endforeach;
    }
}
