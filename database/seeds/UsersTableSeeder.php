<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = 
        [
                    [
                        "username" =>"Fabrice",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                        "user_type_id"=>"1",
                        "kid_id"=>"1",
                        "password" =>"123",
                    ],

                    [
                        "username" =>"Tini",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                        "user_type_id"=>"1",
                        "kid_id"=>"1",
                        "password" =>"123",
                    ],

                    [
                        "username" =>"Tea",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                        "user_type_id"=>"1",
                        "kid_id"=>"2",
                        "password" =>"123",
                    ],
                    [
                        "username" =>"Teremu",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                        "user_type_id"=>"1",
                        "kid_id"=>"3",
                        "password" =>"123",
                    ]
                    
        ];

        foreach($users AS $user):
            $user['password'] = hash::make("secret");
            User::create($user);
        endforeach;
    }

}
