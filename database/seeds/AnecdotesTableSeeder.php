<?php

use Illuminate\Database\Seeder;
use App\Anecdote;

class AnecdotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $anecdotes = 
        [
                    [
                        "description" =>"il roule enfin en roue libre",
                        "image"=>"1",
                        "video"=>"1",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                    ],

                    [
                        "description" =>"il roule enfin en roue libre",
                        "image"=>"1",
                        "video"=>"1",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                    ],

                    [
                        "description" =>"il a fait un avion en papier",
                        "image"=>"1",
                        "video"=>"1",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                    ],
                    [
                        "description" =>"il code sa première api",
                        "image"=>"1",
                        "video"=>"1",
                        "created_by"=>"1",
                        "updated_by"=>"1",
                        "deleted_by"=>"1",
                    ]
                    
        ];

        foreach($anecdotes AS $anecdote):
            Anecdote::create($anecdote);
        endforeach;
    }
}
